# EasyWeb 前端页面


## 1.简介

> 基于jquery、layui的管理系统模板，单页面、响应式、支持mvvm、极易上手！

* 演示地址：[https://whvse.gitee.io/easywebpage/](https://whvse.gitee.io/easywebpage/login.html)

* 演示账号：随便输 &emsp;&emsp; 密码：随便输 

* 开发文档：[https://whvse.gitee.io/easywebpage/docs/](https://whvse.gitee.io/easywebpage/docs/)

* [EasyWeb后台](https://gitee.com/whvse/EasyWeb)，后台基于springboot、Security、OAuth2等。

## 使用框架 

描述 | 框架 
:---|:---
核心框架 | [Layui](http://www.layui.com/)、[jQuery](http://jquery.cuishifeng.cn/)
路由框架 | [Q.js](https://github.com/itorr/q.js) (纯js轻量级路由框架)
mvvm框架 | [pandyle.js](https://gitee.com/pandarrr/pandyle) (专为jquery编写的mvvm)
主要特色 | 单页面 / 响应式 / 简约 / 极易上手

## 项目结构

```
|-assets
|     |-css                     // 样式
|     |-images                  // 图片
|     |-libs                    // 第三方库
|
|-components            // html组件
|     |-system                  // 系统管理页面
|     |-xxxxxx                  // 其他业务页面
|     |-tpl                     // 公用组件
|     |     |-message.html                 // 消息
|     |-console.html            // 主页一
|     |-header.html             // 头部
|     |-side.html               // 侧导航
|
|-module                // js模块 (使用layui的模块开发方式)
|     |-admin.js                // admin模块
|     |-config.js                // config模块
|     |-index.js                // index模块
|
|-index.html            // 主界面
|-login.html            // 登陆界面
```

## 项目截图

![主页一](https://images.gitee.com/uploads/images/2019/1203/143455_b5477937_699620.jpeg)

![消息弹窗](https://images.gitee.com/uploads/images/2019/1203/143455_78ebfab0_699620.jpeg)

![角色管理](https://images.gitee.com/uploads/images/2019/1203/143455_e812c2c4_699620.jpeg)


---

## 联系方式
### 欢迎加入“前后端分离技术交流群”：
![群二维码](https://images.gitee.com/uploads/images/2019/1203/143455_239a53a3_699620.jpeg)

### 我要打赏：
都是猿友，撸码不易，如果这个轮子对你有用，不妨打赏一下！
